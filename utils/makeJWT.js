const jwt = require('jsonwebtoken');

async function makeJWT (data) {
    try {
        return await jwt.sign(
            JSON.stringify(data),
            process.env.SESSION_SECRET
        );
    } catch (err) {
        console.log(err.message);
        return "";
    }
}

module.exports = makeJWT;
