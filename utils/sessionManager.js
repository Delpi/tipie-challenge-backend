const uuid = require('uuid').v4;

class Session {
    constructor (user) {
        this.user = user;
        this.id = uuid();
    }

    compareId (id) {
        return this.id === id;
    }
}

const SessionManager = {
    sessions: [],
    add: function (user) {
        const newSession = new Session(user);
        this.sessions.push(newSession);
        return newSession;
    },
    kill: function (id) {
        this.sessions = this.sessions.filter(element => {
            return !element.compareId(id);
        });
    },
    get: function (id) {
        return this.sessions.find(element => {
            return element.compareId(id);
        });
    }
};

module.exports = SessionManager;
