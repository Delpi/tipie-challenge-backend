const jwt = require('jsonwebtoken');

async function decryptJWT (cookie) {
    try {
        return await jwt.verify(
            cookie,
            process.env.SESSION_SECRET
        );
    } catch (err) {
        console.log(err.message);
        return null;
    }
}

module.exports = decryptJWT;
