const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
require('dotenv').config();

const settings = require('./settings.json');

const login = require('./routes/login.js');
const table = require('./routes/fetchTable.js');

console.log("Starting...");
const app = express();

// CORS
app.use(
    cors({
        origin: (origin, callback) => {
            if (!origin) {
                return callback(null, true);
            }
            if (settings.frontend.indexOf(origin) === -1) {
                return callback(
                    new Error("CORS forbids requests from this origin"),
                    false
                );
            }
            return callback(null, true);
        },
        credentials: true,
    })
);

// Setup to read request body
app.use(
    express.urlencoded({
        extended: true
    })
);
app.use(
    express.json()
);

// Setup to read cookies
app.use(
    cookieParser()
);

// Launching
app.listen(settings.port, () => {
    console.log("Ready!");
});

// Endpoints
app.post('/login', login);
app.get('/table', table);
