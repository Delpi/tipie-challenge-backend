const bcrypt = require('bcryptjs');
const SessionManager = require('../utils/sessionManager.js');

// Data simulacrum
const accounts = require('../data/accounts.json');

async function authenticate (user, password) {
    try {
        const storedUser = await fetchUser(user);
        if (!storedUser) {
            throw new Error("Unable to find user");
        }

        const check = await bcrypt.compare(
            password,
            storedUser.hash
        );
        if (!check) {
            throw new Error("Invalid password");
        }

        return SessionManager.add(user);
    } catch (err) {
        console.log(err.message);
        return false;
    }
}

async function fetchUser (name) {
    // TODO: replace with DB query
    // Must be able to retrieve at least the hashed password for an username
    try {
        const user = accounts.find(account => {
            return account.user === name;
        });
        if (!user) {
            throw new Error("No user found");
            // NOTE: different from a DB lookup error
        }
        return user;
    } catch (err) {
        console.log(err.message);
        return null;
    }
}

module.exports = authenticate;
