const SessionManager = require('../utils/sessionManager.js');

async function authorize (session) {
    try {
        if (!session) {
            throw new Error("No session found");
        }

        const sessionRecord = SessionManager.get(session.id);
        if (!sessionRecord || sessionRecord.user !== session.user) {
            throw new Error("Invalid session");
        }

        return true;
    } catch (err) {
        console.log(err.message);
        return false;
    }
}

module.exports = authorize;
