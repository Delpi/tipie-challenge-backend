const authorize = require('../auth/authorize.js');
const decryptJWT = require('../utils/decryptJWT.js');

// NOTE: to be removed when the table is pulled from DB
const table = require('../data/data_tipie.json');

async function fetchTable (req, res) {
    const authCheck = await authorize(
        await decryptJWT(req.cookies.session)
    );

    if (!authCheck) {
        res.status(401)
            .json({
                status: "No valid session found",
            });
        return;
    }

    const remoteTable = await fetchRemoteTable();
    if (!remoteTable) {
        res.status(500)
            .json({
                status: "Server error: unable to retrieve data"
            });
        return;
    }

    res.json({
        status: "success",
        body: remoteTable
    });
}

async function fetchRemoteTable () {
    // TODO: replace with DB query
    // return null or falsey on error
    return table;
}

module.exports = fetchTable;
