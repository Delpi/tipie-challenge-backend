const authenticate = require('../auth/authenticate.js');
const makeJWT = require('../utils/makeJWT.js');

async function login (req, res) {
    const authCheck = await authenticate(
        req.body.user,
        req.body.password
    );

    if (!authCheck) {
        res.status(401)
            .json({
                status: "Unable to log in"
            });
        return;
    }

    const sessionJWT = await makeJWT(authCheck);
    res.cookie(
        'session',
        sessionJWT,
        {
            secure: true,
            httpOnly: true,
            sameSite: 'none'
        }
    );
    res.json({
        status: "success"
    });
}

module.exports = login;
