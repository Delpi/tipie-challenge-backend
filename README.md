# Tipie Challenge

## Backend guide
### 1. Installation
**This guide assumes a working installation of Node.js in your system, and a basic understanding of terminal operation.**

First, clone the repo with `git clone https://gitlab.com/Delpi/tipie-challenge-backend.git`.

Then, `cd` into your working directoy, and run `npm install` to update all relevant Node packages.

### 2. Initial configuration
Open the `.env` file in any text editor and modify the session secret stored there.

Finally, open the `settings.json` file into any text editor and configure your preferred port and the URL of the frontend of the webapp. Be sure to clarify both http and https, as the CORS middleware can't process wildcards.

### 3. Running
Just launch the server with `npm start`.

## Endpoints
### 1. /login (POST)
Expects a body, encoded as application/json, with fields `user` and `password` containing the user credentials.

If the credentials are valid, returns a JSON object with a field `status` holding "success".

Else, returns error code 401 along with a JSON object with a field `status` holding "Unable to log in".

### 2. /table (GET)
Expects no arguments.

If authorized via JWT returns a JSON object with a field `status` holding "success", and a field `body` holding an array of objects.

If unauthorized, returns error code 401, along with a JSON object with a `status` field detailing the issue.

On failure to retrieve the table, returns error code 500, along with a JSON object with a `status` field detailing the issue.

## Operational principle
To authenticate users, this server stores session information in-memory with the understanding that all sessions would become invalid upon closing it, as the unique session IDs would be lost.

## Design constraints
This server was written under the assumption that the provided data is a simulacrum of a DB without regard for its probable operation. The functions used to pull the files are therefore meant to be rewritten in- place as the DB is connected.
